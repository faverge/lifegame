#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#if defined(WITH_SDL)
#include <SDL/SDL.h>
#endif

/**
 * @brief Defines if a cell is alive or dead
 */
#define DEAD  0
#define ALIVE 1

/**
 * @brief Macro to ease the access to the (i,j) cell in the board
 */
#define cell( _i_, _j_ ) board[ ldboard * (_j_) + (_i_) ]

/**
 * @brief Macro to ease the access to the number of neighbors of the (i,j) cell
 */
#define ngb( _i_, _j_ )  nbngb[ ldnbngb * ((_j_) - 1) + ((_i_) - 1) ]

/**
 * @brief Helper to time the program
 */
double mytimer(void)
{
    struct timeval tp;
    gettimeofday( &tp, NULL );
    return tp.tv_sec + 1e-6 * tp.tv_usec;
}

/**
 * @brief Output the state of the board into the file board-X.txt, where X
 * is replace by the loop index
 */
void boardOutputFile( int N, int *board, int ldboard, int loop )
{
    char *filename;
    FILE *f;
    int i, j, rc;

    rc = asprintf( &filename, "board-%d.txt", loop );
    f = fopen( filename, "w" );
    free( filename );

    fprintf(f, "loop %d\n", loop );
    for (i=0; i<N; i++) {
        for (j=0; j<N; j++) {
            if ( cell( i, j ) == 1 )
                fprintf(f, "X");
            else
                fprintf(f, ".");
        }
        fprintf(f, "\n");
    }
    fclose(f);

    (void)rc;
}

/**
 * @brief Initialize the board with a centered cross
 */
int boardGenInitialCross( int N, int *board, int ldboard )
{
    int i, j, num_alive = 0;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            if ( (i == N/2) ||
                 (j == N/2) ) {
                cell(i, j) = ALIVE;
                num_alive ++;
            }
            else {
                cell(i, j) = DEAD;
            }
        }
    }

    return num_alive;
}

/**
 * @brief Generate the board with random cells
 */
int boardGenInitialRandom( int N, int *board, int ldboard )
{
    int i, j, k, max, num_alive = 0;

    max = rand() % ((N * N) / 2 );

    for (k = 0; k < max; k++) {
        i = rand() % N;
        j = rand() % N;

        if ( cell(i, j) == DEAD ) {
            num_alive ++;
            cell(i, j) = ALIVE;
        }
    }

    return num_alive;
}

/**
 * @brief Structure for the parameters of the code
 */
typedef struct options_s {
    int BS;
    int loop;
    int random;
    int output_file;
    int output_sdl;
} options_t;

/**
 * @brief Print usage details of the testings
 */
static void
print_usage(void)
{
    fprintf(stderr,
            "Mandatory argument:\n"
            " -N                : Dimension of the matrix of cells\n"
            " -l --loop         : Number of iterations\n"
            "Optional arguments:\n"
            " -r --random       : Enable the random generator instead of the initial cross (default: Disabled)\n"
            " -f --output-files : Enable to output the grid state in files at each iteration (default: Disabled)\n"
            " -v --no-sdl       : Disable the output with SDL for performance analysis (default: Enable)\n"
            );
}

#define GETOPT_STRING "N:l:rfvh"
static struct option long_options[] =
{
    {"help",         no_argument,        0, 'h'},
    {"N",            required_argument,  0, 'N'},
    {"loop",         required_argument,  0, 'l'},
    {"random",       no_argument,        0, 'r'},
    {"output-files", no_argument,        0, 'f'},
    {"no-sdl",       no_argument,        0, 'v'},
};

/**
 * @brief Parse options
 */
void
parse_arguments( int argc, char **argv, options_t *options )
{
    int opt = 0;
    int c;

    options->BS          = 320;
    options->loop        = 100;
    options->random      = 0;
    options->output_file = 0;
    options->output_sdl  = 1;

    do {
        c = getopt_long_only(argc, argv, GETOPT_STRING,
                             long_options, &opt);
        switch(c)
        {
        case 'N':
            options->BS = atoi(optarg);
            break;
        case 'l':
            options->loop = atoi(optarg);
            break;
        case 'r':
            options->random = 1;
            break;
        case 'f':
            options->output_file = 1;
            break;
        case 'v':
            options->output_sdl = 0;
            break;
        case 'h':
            print_usage();
            exit(0);

        case '?': /* getopt_long already printed an error message. */
            exit(1);

        default:
            break; /* Assume anything else is parsec/mpi stuff */
        }
    } while(-1 != c);
}

#if defined(WITH_SDL)
/**
 * @brief Initialisation de la SDL
 */
SDL_Surface *sdlInit( int size )
{
    SDL_Surface *screen;

    if (SDL_Init(SDL_INIT_VIDEO) == -1) // Demarrage de la SDL. Si erreur :
    {
        fprintf(stderr, "Erreur d'initialisation de la SDL : %s\n", SDL_GetError()); // �criture de l'erreur
        exit(EXIT_FAILURE); // On quitte le programme
    }

    screen = SDL_SetVideoMode( size, size, 32, SDL_HWSURFACE );
    SDL_WM_SetCaption("Jeu de la vie - PRCD", NULL);

    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 255, 255, 255));

    return screen;
}

/**
 * @brief Hold until the window is closed
 */
void sdlPause()
{
    int continuer = 1;
    SDL_Event event;

    while (continuer)
    {
        SDL_WaitEvent(&event);
        switch(event.type)
        {
        case SDL_QUIT:
            continuer = 0;
        }
    }
}
#endif

/**
 * @brief Update the status of the board. Compute the neighbours and update cell states.
 */
int boardUpdate( int N, int *board, int ldboard,
                 int *nbngb, int ldnbngb )
{
    int i, j, num_alive;

    /* Ghost corner */
    cell(   0, 0   ) = cell( N, N );
    cell(   0, N+1 ) = cell( N, 1 );
    cell( N+1, 0   ) = cell( 1, N );
    cell( N+1, N+1 ) = cell( 1, 1 );

    /* Ghost rows and columns */
    for (i = 1; i <= N; i++) {
        cell(   i,   0 ) = cell( i, N );
        cell(   i, N+1 ) = cell( i, 1 );
        cell(   0,   i ) = cell( N, i );
        cell( N+1,   i ) = cell( 1, i );
    }

    /* Update number of neighbours */
    for (j = 1; j <= N; j++) {
        for (i = 1; i <= N; i++) {
            ngb( i, j ) =
                cell( i-1, j-1 ) + cell( i, j-1 ) + cell( i+1, j-1 ) +
                cell( i-1, j   ) +                  cell( i+1, j   ) +
                cell( i-1, j+1 ) + cell( i, j+1 ) + cell( i+1, j+1 );
        }
    }

    /* Update dead or alive state */
    num_alive = 0;
    for (j = 1; j <= N; j++) {
        for (i = 1; i <= N; i++) {
            /* Kill a cell by isolation of suffocation */
            if ( (ngb( i, j ) < 2) ||
                 (ngb( i, j ) > 3) ) {
                cell(i, j) = DEAD;
            }
            else {
                /* New born */
                if ( ngb( i, j ) == 3 ) {
                    cell(i, j) = ALIVE;
                }
            }

            if (cell(i, j) == ALIVE) {
                num_alive ++;
            }
        }
    }

    return num_alive;
}

int main(int argc, char* argv[])
{
    options_t options;
    int loop, num_alive, maxloop;
    int ldboard, ldnbngb, BS;
    double t1, t2;
    double sumt  = 0.;
    double sumt2 = 0.;

    int *board;
    int *nbngb;

#if defined(WITH_SDL)
    SDL_Surface *screen = NULL;
    SDL_Surface *surface = NULL;
    SDL_Rect position = { .x = 0, .y = 0 };
#endif

    parse_arguments( argc, argv, &options );

    maxloop = options.loop;
    BS      = options.BS;
    num_alive = 0;

    /* Leading dimension of the board array (including ghost regions) */
    ldboard = BS + 2;
    /* Leading dimension of the neigbour counters array */
    ldnbngb = BS;

    board = malloc( ldboard * ldboard * sizeof(int) );
    nbngb = malloc( ldnbngb * ldnbngb * sizeof(int) );

    // Set everyone dead
    memset( board, 0, ldboard * ldboard * sizeof(int) );

    if ( options.random ) {
        num_alive = boardGenInitialRandom( BS, &(cell(1, 1)), ldboard );
    }
    else {
        num_alive = boardGenInitialCross( BS, &(cell(1, 1)), ldboard );
    }
#if defined(WITH_SDL)
    if ( options.output_sdl ) {
        screen = sdlInit( ldboard );
        surface = SDL_CreateRGBSurfaceFrom((void*)board, ldboard, ldboard,
                                           32,                    // bits per pixel = 32
                                           ldboard * sizeof(int), // pitch
                                           0x1,                   // red mask
                                           0x1,                   // green mask
                                           0x1,                   // blue mask
                                           0);                    // alpha mask (none)
        SDL_BlitSurface( surface, NULL, screen, &position );
        SDL_Flip(screen); /* Update screen */
    }
#endif

    printf("Starting number of living cells = %d\n", num_alive);

    for (loop = 0; loop < maxloop; loop++) {

        t1 = mytimer();
        num_alive = boardUpdate( BS, board, ldboard, nbngb, ldnbngb );
        t2 = mytimer();
        sumt  += (t2 - t1) * 1e3;
        sumt2 += (t2 - t1) * (t2 - t1) * 1e6;

        /* Output board into a file for debug */
        if ( options.output_file ) {
            /* Let's include ghost regions for debug */
            boardOutputFile( BS+2, &(cell(0, 0)), ldboard, loop );

            /* Or just with the real cells, we start at (1,1) */
            //boardOutputFile( BS, &(cell(1, 1)), ldboard, loop+1);
        }

        printf( "%05d: %5d cells are alive\n", loop, num_alive );

#if defined(WITH_SDL)
        if ( options.output_sdl ) {
            SDL_BlitSurface( surface, NULL, screen, &position );
            SDL_Flip(screen); /* Update screen */
        }
#endif
    }

    printf("Final number of living cells = %d\n"
           "Total time: %.2e ms\n"
           "Average time per iteration: %.2lf ms (+/- %.2lf ms )\n",
           num_alive, sumt, sumt / maxloop,
           sqrt( (sumt2 - (sumt * sumt) / maxloop ) / maxloop) );

#if defined(WITH_SDL)
    if ( options.output_sdl ) {
        sdlPause();
        SDL_FreeSurface( surface );
        SDL_Quit();
    }
#endif

    free(board);
    free(nbngb);

    return EXIT_SUCCESS;
}
