CFLAGS=-Wall -O3
LDFLAGS=-lm

CFLAGS_SDL=$(CFLAGS) -DWITH_SDL
LDFLAGS_SDL=$(LDFLAGS) -lSDL

default: life_seq life_seq_sdl

life_seq: life_seq.o
	$(CC) -o $@ $^ $(LDFLAGS)

life_seq_sdl: life_seq_sdl.o
	$(CC) -o $@ $^ $(LDFLAGS_SDL)

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS)

%_sdl.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS_SDL)

clean:
	rm -f *.o *~

cleanall: clean
	rm -f life_seq life_seq_sdl
